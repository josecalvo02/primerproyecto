package Views;

import Controller.Games.*;
import Controller.Rankings.AddRanking;
import Controller.WindowMaintenance.RefreshWindowController;
import Model.DBAmountOfClicks;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GameWindow extends JFrame implements ActionListener, Runnable  {
    public String orderGame = "";
    Integer minuts = 0;
    Integer seconds = 0;
    Integer miliseconds = 0;
    
    public GameWindow(String orderGame){
        this.orderGame = orderGame;
        initComponents();
        this.setTitle("Puzzle Game");
        this.setSize(600,500);
        this.setLocationRelativeTo(null);
    }
    
    private void initComponents(){
        //We declare the clases
        shuffle = new ShuffleGameController();
        solvable = new IsSolvableGameController();
        images = new SetImagesGameController();
        refresh = new RefreshWindowController();

        //Panel for the game
        leftPanel = new JPanel();     
        leftPanel.setBackground(Color.white);
        leftPanel.setSize(500, 500);
        leftPanel.setLayout(new GridLayout(4,4));
        
        //Panel for the components
        rightPanel = new JPanel();
        rightPanel.setBackground(Color.LIGHT_GRAY);
        rightPanel.setSize(100, 500);
        rightPanel.setLayout(null);
        addRightComponents();
        
        startChronometer();
        
        //Split the panel in half
        JSplitPane pane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel );
        
        //Functional pane
        this.add(pane);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Starting vars
        rows = 4;
        cols = 4;
        board = new int[rows][cols];
        button = new JButton[rows][cols];
        label  = new JLabel[rows][cols];

        //Here we search for the order chosen to shuffle the game
        if(orderGame.equals("Ascending")){
            //This do-while repets until is solvable
            do {
                shuffle.shuffleBoard(board, cols, rows);
            } while(!solvable.isSolvable(board, cols, rows));
        }
        else{
            //This do-while repets until is solvable
            do {
                shuffle.shuffleBoardTwo(board, cols, rows);
            } while(!solvable.isSolvable(board, cols, rows));
        }
        //This method sets the images
        images.setImages(this , leftPanel, button, label, board, cols, rows); 
    }

    @Override
    public void actionPerformed(ActionEvent ae){
        check = new CheckGameController();
        Boolean flag;
        
        if(orderGame.equals("Ascending"))
            flag = check.isSolved(board, cols, rows);
        else
            flag = check.isSolvedTwo(board, cols, rows);
        
        if(flag == false){
            String s = ae.getActionCommand();
            int row = Integer.parseInt(s.split(",")[0]);
            int col = Integer.parseInt(s.split(",")[1]);

            if(board[row][col]!= -1){
                // Check if it is possible to move up
                if(row+1 < rows && board[row+1][col] == -1){
                    label[row][col].setIcon(new ImageIcon(""));
                    String fileName = "Pics/" + board[row][col] + ".png";
                    label[row+1][col].setIcon(new ImageIcon(fileName));
                    int temp = board[row][col];
                    board[row][col] = board[row+1][col];
                    board[row+1][col] = temp;
                    DBAmountOfClicks.corClicks++;
                    this.labelNumOne.setText(Integer.toString(DBAmountOfClicks.corClicks));
                    refresh.refreshFrame(this);
                }
                // Check if it is possible to move down
                else if(row-1 >= 0 && board[row-1][col] == -1){
                    label[row][col].setIcon(new ImageIcon(""));
                    String fileName = "Pics/" + board[row][col] + ".png";
                    label[row-1][col].setIcon(new ImageIcon(fileName));
                    int temp = board[row][col];
                    board[row][col] = board[row-1][col];
                    board[row-1][col] = temp;
                    DBAmountOfClicks.corClicks++;
                    this.labelNumOne.setText(Integer.toString(DBAmountOfClicks.corClicks));
                    refresh.refreshFrame(this);
                }
                // Check if it is possible to move right
                else if(col+1 < cols && board[row][col+1] == -1){
                    label[row][col].setIcon(new ImageIcon(""));
                    String fileName = "Pics/" + board[row][col] + ".png";
                    label[row][col+1].setIcon(new ImageIcon(fileName));
                    int temp = board[row][col];
                    board[row][col] = board[row][col+1];
                    board[row][col+1] = temp;
                    DBAmountOfClicks.corClicks++;
                    this.labelNumOne.setText(Integer.toString(DBAmountOfClicks.corClicks));
                    refresh.refreshFrame(this);
                }
                // Check if it is possible to move left
                else if(col-1 >= 0 && board[row][col-1]== -1){
                    label[row][col].setIcon(new ImageIcon(""));
                    String fileName = "Pics/" + board[row][col] + ".png";
                    label[row][col-1].setIcon(new ImageIcon(fileName));
                    int temp = board[row][col];
                    board[row][col] = board[row][col-1];
                    board[row][col-1] = temp;
                    DBAmountOfClicks.corClicks++;
                    this.labelNumOne.setText(Integer.toString(DBAmountOfClicks.corClicks));
                    refresh.refreshFrame(this);
                }
                else{
                    DBAmountOfClicks.incClicks++;
                    this.labelNumTwo.setText(Integer.toString(DBAmountOfClicks.incClicks));
                    refresh.refreshFrame(this);
                }
            }
            
            if(orderGame.equals("Ascending"))
                flag = check.isSolved(board, cols, rows);
            else
                flag = check.isSolvedTwo(board, cols, rows);
            
            //It is displayed when you win the game
            if(flag == true){
                //We add a new game to the matrix
                int [][] numGames = new int[MainFile.users.length][DBAmountOfClicks.numOfGames[MainFile.userCode].length+1];
                DBAmountOfClicks.numOfGames[MainFile.userCode] = numGames[MainFile.userCode];

                //We get the position of the current game and position of the array
                int numLength = DBAmountOfClicks.numOfGames[MainFile.userCode].length;
                int numLengthTwo = DBAmountOfClicks.numOfGames[MainFile.userCode].length-1;

                //We add a temporal matrix for clicks
                int[][][] countClicks = new int [MainFile.users.length][numLength][numLength];
                int[][][] countIncClicks = new int [MainFile.users.length][numLength][numLength];

                //We add the number of correct clicks to the tmp matrix then to the correct one
                countClicks[MainFile.userCode][numLengthTwo][numLengthTwo] = DBAmountOfClicks.corClicks;
                DBAmountOfClicks.counterOfClicks = countClicks;

                //We add the number of correct clicks to the tmp matrix then to the correct one
                countIncClicks[MainFile.userCode][numLengthTwo][numLengthTwo] = DBAmountOfClicks.incClicks;
                DBAmountOfClicks.counterOfWrongClicks = countIncClicks;

                //We set the clicks to 0
                DBAmountOfClicks.corClicks = 0;
                DBAmountOfClicks.incClicks = 0;

                //We create some tmp matix to save the time
                int[][][] countMin = new int [MainFile.users.length][numLength][numLength];
                int[][][] countSec = new int [MainFile.users.length][numLength][numLength];
                
                //We get the minutes and add it to a tmp matrix
                countMin[MainFile.userCode][numLengthTwo][numLengthTwo] = minuts;
                countSec[MainFile.userCode][numLengthTwo][numLengthTwo] = seconds;
                
                //We set the minutes to the correct matrix
                DBAmountOfClicks.counterOfMin = countMin;
                DBAmountOfClicks.counterOfSec = countSec;
               
                stopChronometer();
                
                add = new AddRanking();
                add.addNewRanking(numLengthTwo, DBAmountOfClicks.counterOfMin[MainFile.userCode][numLengthTwo][numLengthTwo],DBAmountOfClicks.counterOfSec[MainFile.userCode][numLengthTwo][numLengthTwo],"",0);
                
                displayWinMsg();
            }
        }
    }

    //This method adds the components for the right panel
    public void addRightComponents(){
        //Declaring components
        btnGiveUp = new javax.swing.JButton("Give Up");
        labelCorClicks = new javax.swing.JLabel("Correct Clicks");
        labelIncClicks = new javax.swing.JLabel("Wrong Clicks");
        labelNumOne = new javax.swing.JLabel(Integer.toString(DBAmountOfClicks.corClicks));
        labelNumTwo = new javax.swing.JLabel(Integer.toString(DBAmountOfClicks.incClicks));
        labelUsernameGame = new javax.swing.JLabel(MainFile.users[MainFile.userCode][1] , SwingConstants.CENTER);
        tiempo = new JLabel( "00:00:000" );
        
        //Setting properties for the chronometer label
        tiempo.setFont( new Font( Font.SERIF, Font.BOLD, 20 ) );
        tiempo.setHorizontalAlignment( JLabel.CENTER );
        tiempo.setOpaque( true );
        tiempo.setBounds(3, 250, 150, 30);
        rightPanel.add( tiempo );
        
         //Setting properties for the labels
         labelCorClicks.setFont(new Font("TimesRoman",Font.BOLD,20));
         labelCorClicks.setName("labelCorClicks");
         rightPanel.add(labelCorClicks);
         labelCorClicks.setBounds(15, 70, 150, 30);
         labelCorClicks.setVerticalAlignment(SwingConstants.CENTER);
         
         labelIncClicks.setFont(new Font("TimesRoman",Font.BOLD,20));
         labelIncClicks.setName("labelIncClicks");
         rightPanel.add(labelIncClicks);
         labelIncClicks.setBounds(20, 170, 150, 30);
         labelIncClicks.setVerticalAlignment(SwingConstants.CENTER);
         
         labelNumOne.setFont(new Font("TimesRoman",Font.BOLD,20));
         labelNumOne.setName("labelNumOne");
         rightPanel.add(labelNumOne);
         labelNumOne.setBounds(70, 100, 150, 30);
         
         labelNumTwo.setFont(new Font("TimesRoman",Font.BOLD,20));
         labelNumTwo.setName("labelNumTwo");
         rightPanel.add(labelNumTwo);
         labelNumTwo.setBounds(70, 200, 150, 30);
         
         labelUsernameGame.setFont(new Font("TimesRoman",Font.BOLD,20));
         labelUsernameGame.setName("labelUsername");
         rightPanel.add(labelUsernameGame);
         labelUsernameGame.setBounds(10, 10, 135, 30);
         //labelUsernameGame.setVerticalAlignment(SwingConstants.CENTER);
         
         //Setting porperties for the btn
         btnGiveUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGiveUpActionPerformed(evt);
            }
        });
         btnGiveUp.setBounds(20, 400, 120, 30);
         btnGiveUp.setBackground(Color.WHITE);
         rightPanel.add(btnGiveUp);
    }
    
    //This method is called by the give up btn
    private void btnGiveUpActionPerformed(java.awt.event.ActionEvent evt) {                                          
        refresh = new RefreshWindowController();
        solve = new SolveGameController();
        get = new GetRightPositions();
        int countTries = 0;
        
        if(orderGame.equals("Ascending")){
            countTries = get.getRightPos(board,cols,rows);
            solve.solveGame(board, cols, rows);
            images.setImagesSolvedOne(this, leftPanel, button, label, board, cols, rows);
        }
        else{
            countTries = get.getRightPosTwo(board,cols,rows);
            solve.solveGameTwo(board, cols, rows);
            images.setImagesSolvedTwo(this, leftPanel, button, label, board, cols, rows);
        }
        
        //We add a new game to the matrix
        int [][] numGames = new int[MainFile.users.length][DBAmountOfClicks.numOfGames[MainFile.userCode].length+1];
        DBAmountOfClicks.numOfGames[MainFile.userCode] = numGames[MainFile.userCode];

        //We get the position of the current game and position of the array
        int numLength = DBAmountOfClicks.numOfGames[MainFile.userCode].length;
        int numLengthTwo = DBAmountOfClicks.numOfGames[MainFile.userCode].length-1;

        //We add a temporal matrix for clicks
        int[][][] countClicks = new int [MainFile.users.length][numLength][numLength];
        int[][][] countIncClicks = new int [MainFile.users.length][numLength][numLength];

        //We add the number of correct clicks to the tmp matrix then to the correct one
        countClicks[MainFile.userCode][numLengthTwo][numLengthTwo] = DBAmountOfClicks.corClicks;
        DBAmountOfClicks.counterOfClicks = countClicks;

        //We add the number of correct clicks to the tmp matrix then to the correct one
        countIncClicks[MainFile.userCode][numLengthTwo][numLengthTwo] = DBAmountOfClicks.incClicks;
        DBAmountOfClicks.counterOfWrongClicks = countIncClicks;

        //We set the clicks to 0
        DBAmountOfClicks.corClicks = 0;
        DBAmountOfClicks.incClicks = 0;

        //We create some tmp matix to save the time
        int[][][] countMin = new int [MainFile.users.length][numLength][numLength];
        int[][][] countSec = new int [MainFile.users.length][numLength][numLength];
                
        //We get the minutes and add it to a tmp matrix
        countMin[MainFile.userCode][numLengthTwo][numLengthTwo] = minuts;
        countSec[MainFile.userCode][numLengthTwo][numLengthTwo] = seconds;
                
        //We set the minutes to the correct matrix
        DBAmountOfClicks.counterOfMin = countMin;
        DBAmountOfClicks.counterOfSec = countSec;
        
        stopChronometer();
        
        String giveUp = "(Give Up)";
        add = new AddRanking();
        add.addNewRanking(numLengthTwo, DBAmountOfClicks.counterOfMin[MainFile.userCode][numLengthTwo][numLengthTwo],DBAmountOfClicks.counterOfSec[MainFile.userCode][numLengthTwo][numLengthTwo],giveUp, countTries);
                
        refresh.refreshFrame(this);
        
        JOptionPane.showMessageDialog(null, "Dont worry, next time you will make it");
        UserLogedWindow logedWindow = new UserLogedWindow();
        logedWindow.setLocationRelativeTo(null);
        logedWindow.setVisible(true);
        this.setVisible(false);
    }
    
    //This method displayes the win frame
    public void displayWinMsg(){
        JFrame frame = new JFrame("Game Win");
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setSize(390,200);
        frame.setBackground(Color.white);
        
        btnWin= new javax.swing.JButton("Accept");
        labelWin = new javax.swing.JLabel("Congrats! you solve The Puzzle");
        
        //Functional pane
        frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
        //Label
        labelWin.setFont(new Font("TimesRoman",Font.BOLD,20));
        labelWin.setName("labelWin");
        frame.getContentPane().add(labelWin);
        labelWin.setBounds(50, 30, 300, 30);
         
        //Button
        btnWin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWinActionPerformed(evt,frame);
            }
        });
        frame.getContentPane().add(btnWin);
        btnWin.setBounds(120, 70, 120, 30);
        btnWin.setBackground(Color.WHITE);
    }
    
    //The method is called by the accept btn
    private void btnWinActionPerformed(java.awt.event.ActionEvent evt, JFrame frame) {                                          
        UserLogedWindow logedWindow = new UserLogedWindow();
        logedWindow.setLocationRelativeTo(null);
        logedWindow.setVisible(true);
        this.setVisible(false);
        frame.setVisible(false);
    }
    
    //This method runs the chronometer
    public void run(){
        minuts = 0;
        seconds = 0;
        miliseconds = 0;
        
        String min="";
        String seg = "";
        String mil = "";
        
        try{
            //While the flag is true it will keep counting
            while( cronometroActivo ){
                Thread.sleep( 4 );
                miliseconds += 4;

                if( miliseconds == 1000 ){
                    miliseconds = 0;
                    seconds += 1;
                    
                    if( seconds == 60 ){
                        seconds = 0;
                        minuts++;
                    }
                }

                if( minuts < 10 ) 
                    min = "0" + minuts;
                else 
                    min = minuts.toString();
                if( seconds < 10 ) 
                    seg = "0" + seconds;
                else 
                    seg = seconds.toString();

                if( miliseconds < 10 ) 
                    mil = "00" + miliseconds;
                else if( miliseconds < 100 ) 
                    mil = "0" + miliseconds;
                else 
                    mil = miliseconds.toString();
                
                //We establish the time
                tiempo.setText( min + ":" + seg + ":" + mil );
                
            }
        }
        catch(Exception e){
        }
        
        tiempo.setText( "00:00:000" );
    }
    
    //This method starts the chronometer
    public void startChronometer() {
        cronometroActivo = true;
        hilo = new Thread( this );
        hilo.start();
    }

    //This method stops the chronometer
    public void stopChronometer(){
        cronometroActivo = false;
    }

    //Panels
    JPanel leftPanel;
    JPanel rightPanel;
    
    //Components of the game
    int rows ;
    int cols ;
    int [][] board;
    JButton [][] button;
    JLabel [][] label;
    
    //Components of win frame
    JButton btnWin;
    JLabel labelWin;
    
    //Components of right panel
    JButton btnGiveUp;
    JLabel labelCorClicks;
    JLabel labelIncClicks;
    JLabel labelNumOne;
    JLabel labelNumTwo;
    JLabel labelUsernameGame;
    
    //Components ot the chronometer
    JLabel tiempo;
    Thread hilo;
    boolean cronometroActivo;
    
    //Vars to call classes
    ShuffleGameController shuffle;
    CheckGameController check;
    IsSolvableGameController solvable;
    SetImagesGameController images;
    RefreshWindowController refresh;
    SolveGameController solve;
    AddRanking add;
    GetRightPositions get;
}     