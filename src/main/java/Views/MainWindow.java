package Views;

import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
    public MainWindow () {
        initComponents();
        this.setSize(400, 320);
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setTitle("Bienvenido!");
    }
    
    private void initComponents() {
        //Necessary buttons
        btnRegister = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        
        //Functional pane
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        
        //This method initializes the buttons
        initializeBtns();
    }
    
    //This method is started by btn Login
    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {                                          
        LoginWindow loWindow = new LoginWindow();
        loWindow.setLocationRelativeTo(null);
        loWindow.setVisible(true);
        this.setVisible(false);
    }
    
    //This method is started by btn Register
    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {                                          
        RegisterWindow reWindow = new RegisterWindow();
        reWindow.setLocationRelativeTo(null);
        reWindow.setVisible(true);
        this.setVisible(false);
    }
    
    //Declaring neccesary buttons
    private javax.swing.JButton btnRegister;
    private javax.swing.JButton btnLogin;
    
    //This method initializes the buttons
    private void initializeBtns() {
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin);
        btnLogin.setBounds(40, 100, 120, 30);
        btnLogin.setBackground(Color.WHITE);
        
        btnRegister.setText("Register");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });
        getContentPane().add(btnRegister);
        btnRegister.setBounds(225, 100, 120, 30);
        btnRegister.setBackground(Color.WHITE);
    }
}
