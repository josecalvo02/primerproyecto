package Views;

import static Model.DBRanking.*;
import java.awt.Color;

public class RankingWindow extends javax.swing.JFrame {
    public RankingWindow() {
        initComponents();
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setTitle("Ranking de partidas");
        this.setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        tablePanel = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnReturn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        scrollPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setToolTipText("");
        scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setViewportBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tablePanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        tablePanel.setFont(new java.awt.Font("Times New Roman", 0, 14));
        tablePanel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {1, topTenRankingUser[0][0]+topTenRankingUser[0][1], topTenRanking[0][0]+":"+topTenRanking[0][1], topTenRanking[0][2], topTenRanking[0][3], topTenRanking[0][4], topTenRanking[0][5]},
                {2, topTenRankingUser[1][0]+topTenRankingUser[1][1], topTenRanking[1][0]+":"+topTenRanking[1][1], topTenRanking[1][2], topTenRanking[1][3], topTenRanking[1][4], topTenRanking[1][5]},
                {3, topTenRankingUser[2][0]+topTenRankingUser[2][1], topTenRanking[2][0]+":"+topTenRanking[2][1], topTenRanking[2][2], topTenRanking[2][3], topTenRanking[2][4], topTenRanking[2][5]},
                {4, topTenRankingUser[3][0]+topTenRankingUser[3][1], topTenRanking[3][0]+":"+topTenRanking[3][1], topTenRanking[3][2], topTenRanking[3][3], topTenRanking[3][4], topTenRanking[3][5]},
                {5, topTenRankingUser[4][0]+topTenRankingUser[4][1], topTenRanking[4][0]+":"+topTenRanking[4][1], topTenRanking[4][2], topTenRanking[4][3], topTenRanking[4][4], topTenRanking[4][5]},
                {6, topTenRankingUser[5][0]+topTenRankingUser[5][1], topTenRanking[5][0]+":"+topTenRanking[5][1], topTenRanking[5][2], topTenRanking[5][3], topTenRanking[5][4], topTenRanking[5][5]},
                {7, topTenRankingUser[6][0]+topTenRankingUser[6][1], topTenRanking[6][0]+":"+topTenRanking[6][1], topTenRanking[6][2], topTenRanking[6][3], topTenRanking[6][4], topTenRanking[6][5]},
                {8, topTenRankingUser[7][0]+topTenRankingUser[7][1], topTenRanking[7][0]+":"+topTenRanking[7][1], topTenRanking[7][2], topTenRanking[7][3], topTenRanking[7][4], topTenRanking[7][5]},
                {9, topTenRankingUser[8][0]+topTenRankingUser[8][1], topTenRanking[8][0]+":"+topTenRanking[8][1], topTenRanking[8][2], topTenRanking[8][3], topTenRanking[8][4], topTenRanking[8][5]},
                {10,topTenRankingUser[9][0]+topTenRankingUser[9][1], topTenRanking[9][0]+":"+topTenRanking[9][1], topTenRanking[9][2], topTenRanking[9][3], topTenRanking[9][4], topTenRanking[9][5]}
            },
            new String [] {
                "Place", "Username", "Time", "Valid Mov", "Invalid Mov", "Total Mov", "Pts"
            }
        )
        {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        }
    );
    tablePanel.setToolTipText("");
    tablePanel.setRowHeight(25);
    tablePanel.setShowGrid(true);
    tablePanel.getTableHeader().setResizingAllowed(false);
    tablePanel.getTableHeader().setReorderingAllowed(false);
    tablePanel.setVerifyInputWhenFocusTarget(false);
    scrollPane.setViewportView(tablePanel);
    if (tablePanel.getColumnModel().getColumnCount() > 0) {
        tablePanel.getColumnModel().getColumn(0).setPreferredWidth(20);
        tablePanel.getColumnModel().getColumn(2).setPreferredWidth(30);
        tablePanel.getColumnModel().getColumn(3).setPreferredWidth(30);
        tablePanel.getColumnModel().getColumn(4).setPreferredWidth(30);
        tablePanel.getColumnModel().getColumn(5).setPreferredWidth(30);
        tablePanel.getColumnModel().getColumn(6).setPreferredWidth(20);
    }

    jPanel1.setBackground(new java.awt.Color(204, 204, 204));
    jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

    btnReturn.setText("Return");
    btnReturn.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnReturnActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(btnReturn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(121, 121, 121)
            .addComponent(btnReturn, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
    mainPanel.setLayout(mainPanelLayout);
    mainPanelLayout.setHorizontalGroup(
        mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(mainPanelLayout.createSequentialGroup()
            .addComponent(scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE))
    );
    mainPanelLayout.setVerticalGroup(
        mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(mainPanelLayout.createSequentialGroup()
            .addComponent(scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE))
        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        UserLogedWindow userLoged = new UserLogedWindow();
        userLoged.setLocationRelativeTo(null);
        userLoged.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnReturnActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnReturn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTable tablePanel;
    // End of variables declaration//GEN-END:variables
}
