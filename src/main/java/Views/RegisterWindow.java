package Views;

import Controller.User.CreateNewUser;
import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class RegisterWindow extends JFrame {
    
    public RegisterWindow () {
        initComponents();
        this.setSize(370, 400);
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setTitle("Nuevo Usuario");
    }
    
    private void initComponents() {
        this.newUser = new CreateNewUser();
        
        //Necessary labels, text fields and buttons
        labelName = new javax.swing.JLabel();
        labelUsername = new javax.swing.JLabel();
        labelPassword = new javax.swing.JLabel();
        textFieldName = new javax.swing.JTextField();
        textFieldUsername = new javax.swing.JTextField();
        textFieldPassword = new javax.swing.JTextField();
        btnCreate = new javax.swing.JButton();
        btnGoBack = new javax.swing.JButton();
        
        //Functional pane
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        
        //This method initializes the components of the frame
        initializeComponents();
    }
    
    //This method is started by btn Create
    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {                                          
        if(this.textFieldName.getText().equals("") || this.textFieldUsername.getText().equals("") || this.textFieldPassword.getText().equals(""))
            JOptionPane.showMessageDialog(null, "It is not possible to leave any space in blanck");
        else{
            if(MainFile.code != 0){
                int counter = 0;
                for (int i = 0; i < MainFile.code; i++) {
                    if(MainFile.users[i][1].equals(this.textFieldUsername.getText())){
                        JOptionPane.showMessageDialog(null, "There is already an user created under that username, try a new one");
                        i = MainFile.code + 1;
                        clearTextField();
                    }
                    else
                        counter++;
                }
                if(counter == MainFile.code){
                    this.newUser.createNewUserProfile(
                            this.textFieldName.getText(),
                            this.textFieldUsername.getText(),
                            this.textFieldPassword.getText()
                        );
                    clearTextField();
                    goBack();
                }
            }
            else{
                this.newUser.createNewUserProfile(
                    this.textFieldName.getText(),
                    this.textFieldUsername.getText(),
                    this.textFieldPassword.getText()
                );
                clearTextField();
                goBack();
            }
        }       
    }
    
    //This method returns to main window
    private void btnGoBackActionPerformed(java.awt.event.ActionEvent evt) {                                          
        goBack();
    }
    
    private void clearTextField(){
        this.textFieldName.setText("");
        this.textFieldUsername.setText("");
        this.textFieldPassword.setText("");
    }
    
    private void goBack(){
        MainWindow mainWindow = new MainWindow();
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
        this.setVisible(false);
    }
    
    //Declaring necessary labels, text fields and buttons
    private javax.swing.JLabel labelName;
    private javax.swing.JLabel labelUsername;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JTextField textFieldName;
    private javax.swing.JTextField textFieldUsername;
    private javax.swing.JTextField textFieldPassword;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnGoBack;
    
    CreateNewUser newUser;
    
    //This method initializes the components of the frame
    private void initializeComponents(){
        //Labels properties
        labelName.setText("Name:");
        labelName.setName("labelName");
        getContentPane().add(labelName);
        labelName.setBounds(40, 20, 120, 17);
        
        labelUsername.setText("Username:");
        labelUsername.setName("labelUsername");
        getContentPane().add(labelUsername);
        labelUsername.setBounds(40, 80, 120, 17);
        
        labelPassword.setText("Password:");
        labelPassword.setName("labelPassword");
        getContentPane().add(labelPassword);
        labelPassword.setBounds(40, 140, 120, 17);
        
        //Text fields properties
        textFieldName.setName("textFieldName");
        getContentPane().add(textFieldName);
        textFieldName.setBounds(40, 40, 255, 30);
        textFieldName.setHorizontalAlignment(JTextField.RIGHT);
        
        textFieldUsername.setName("textFieldUsername");
        getContentPane().add(textFieldUsername);
        textFieldUsername.setBounds(40, 100, 255, 30);
        textFieldUsername.setHorizontalAlignment(JTextField.RIGHT);
        
        textFieldPassword.setName("textFieldPassword");
        getContentPane().add(textFieldPassword);
        textFieldPassword.setBounds(40, 160, 255, 30);
        textFieldPassword.setHorizontalAlignment(JTextField.RIGHT);
        
        //Buttons properties
        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });
        getContentPane().add(btnCreate);
        btnCreate.setBounds(40, 260, 120, 30);
        btnCreate.setBackground(Color.WHITE);
        
        btnGoBack.setText("Return");
        btnGoBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoBackActionPerformed(evt);
            }
        });
        getContentPane().add(btnGoBack);
        btnGoBack.setBounds(177, 260, 120, 30);
        btnGoBack.setBackground(Color.WHITE);
    }
}
