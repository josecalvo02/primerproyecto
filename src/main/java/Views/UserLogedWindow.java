package Views;

import Controller.User.*;
import java.awt.Color;
import javax.swing.*;

public class UserLogedWindow extends javax.swing.JFrame {
    public UserLogedWindow() {
        initComponents();
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setTitle("Jugar o administrar usuario");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelUsername = new javax.swing.JLabel();
        btnGoBack = new javax.swing.JButton();
        btnPlay = new javax.swing.JButton();
        comboBoxGameOrder = new javax.swing.JComboBox<>();
        btnRanking = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuBarEdit = new javax.swing.JMenu();
        menuItemShowInfo = new javax.swing.JMenuItem();
        menuItemUpdateInfo = new javax.swing.JMenuItem();
        menuItemDeleteInfo = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelUsername.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelUsername.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelUsername.setText(MainFile.users[MainFile.userCode][1]);

        btnGoBack.setText("Close session");
        btnGoBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoBackActionPerformed(evt);
            }
        });

        btnPlay.setText("Jugar");
        btnPlay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayActionPerformed(evt);
            }
        });

        comboBoxGameOrder.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ascending", "Desending" }));
        comboBoxGameOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxGameOrderActionPerformed(evt);
            }
        });

        btnRanking.setText("Rankings");
        btnRanking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRankingActionPerformed(evt);
            }
        });

        menuBarEdit.setText("Edit  Info");

        menuItemShowInfo.setText("Show User info");
        menuItemShowInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemShowInfoActionPerformed(evt);
            }
        });
        menuBarEdit.add(menuItemShowInfo);

        menuItemUpdateInfo.setText("Update info");
        menuItemUpdateInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemUpdateInfoActionPerformed(evt);
            }
        });
        menuBarEdit.add(menuItemUpdateInfo);

        menuItemDeleteInfo.setText("Delete user");
        menuItemDeleteInfo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemDeleteInfoActionPerformed(evt);
            }
        });
        menuBarEdit.add(menuItemDeleteInfo);

        jMenuBar1.add(menuBarEdit);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnRanking, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGoBack))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(comboBoxGameOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnPlay, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addComponent(labelUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(96, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPlay, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxGameOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRanking, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGoBack, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoBackActionPerformed
        MainWindow mainWindow = new MainWindow();
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnGoBackActionPerformed

    private void menuItemDeleteInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemDeleteInfoActionPerformed
        int confirm = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete it?");
            if(JOptionPane.OK_OPTION == confirm){
                DeleteUserController delete = new DeleteUserController();
                delete.deleteUser();
                JOptionPane.showMessageDialog(null, "Your profile has been deleted");
                MainWindow mainWindow = new MainWindow();
                mainWindow.setLocationRelativeTo(null);
                mainWindow.setVisible(true);
                this.setVisible(false);
            }
            else
                JOptionPane.showMessageDialog(null, "Thank you for keeping your profile!");
    }//GEN-LAST:event_menuItemDeleteInfoActionPerformed

    private void btnPlayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayActionPerformed
        if(this.comboBoxGameOrder.getSelectedItem().equals("Ascending")){
            GameWindow game = new GameWindow((String) this.comboBoxGameOrder.getSelectedItem());
            game.setVisible(true);
            this.setVisible(false);
        }
        else{
            GameWindow game = new GameWindow((String) this.comboBoxGameOrder.getSelectedItem());
            game.setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_btnPlayActionPerformed

    private void comboBoxGameOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxGameOrderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxGameOrderActionPerformed

    private void menuItemShowInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemShowInfoActionPerformed
        JOptionPane.showMessageDialog(null, "Name: " + MainFile.users[MainFile.userCode][0]+"\n"+
                                            "Username: " + MainFile.users[MainFile.userCode][1]
        );
    }//GEN-LAST:event_menuItemShowInfoActionPerformed

    private void menuItemUpdateInfoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemUpdateInfoActionPerformed
        int opt = JOptionPane.showOptionDialog(null,
                                                "Choose the option", 
                                                "Modifying profile",
                                                JOptionPane.YES_NO_CANCEL_OPTION,
                                                JOptionPane.QUESTION_MESSAGE,
                                                null,
                                                new Object[] { "Name", "Username", "Password" },
                                                null
                    );
        UpdateUserController updUser = new UpdateUserController();
        updUser.updateUser(this, this.labelUsername, opt);
    }//GEN-LAST:event_menuItemUpdateInfoActionPerformed

    private void btnRankingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRankingActionPerformed
        RankingWindow rank = new RankingWindow();
        rank.setLocationRelativeTo(null);
        rank.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnRankingActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGoBack;
    private javax.swing.JButton btnPlay;
    private javax.swing.JButton btnRanking;
    private javax.swing.JComboBox<String> comboBoxGameOrder;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JLabel labelUsername;
    private javax.swing.JMenu menuBarEdit;
    private javax.swing.JMenuItem menuItemDeleteInfo;
    private javax.swing.JMenuItem menuItemShowInfo;
    private javax.swing.JMenuItem menuItemUpdateInfo;
    // End of variables declaration//GEN-END:variables
}
