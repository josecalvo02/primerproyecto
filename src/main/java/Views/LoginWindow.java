package Views;

import Controller.User.CreateNewUser;
import java.awt.Color;
import javax.swing.JOptionPane;

public class LoginWindow extends javax.swing.JFrame {

    public LoginWindow() {
        initComponents();
        this.getContentPane().setBackground(Color.LIGHT_GRAY);
        this.setTitle("Iniciar Sesion");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGoBack = new javax.swing.JButton();
        btnLogin = new javax.swing.JButton();
        labelUsername = new javax.swing.JLabel();
        textFieldUsername = new javax.swing.JTextField();
        labelPassword = new javax.swing.JLabel();
        passwordFieldOne = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnGoBack.setText("Return");
        btnGoBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoBackActionPerformed(evt);
            }
        });

        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        labelUsername.setText("Username");

        textFieldUsername.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        labelPassword.setText("Password");

        passwordFieldOne.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        passwordFieldOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordFieldOneActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelUsername)
                                .addGap(18, 18, 18)
                                .addComponent(textFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(64, 64, 64)
                                .addComponent(btnGoBack)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelPassword)
                        .addGap(20, 20, 20)
                        .addComponent(passwordFieldOne, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordFieldOne, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLogin)
                    .addComponent(btnGoBack))
                .addContainerGap(40, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoBackActionPerformed
        MainWindow mainWindow = new MainWindow();
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_btnGoBackActionPerformed

    private void passwordFieldOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordFieldOneActionPerformed
        
    }//GEN-LAST:event_passwordFieldOneActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        this.newUser = new CreateNewUser();
        char[]password;
        String convertedPassword = "";
        password = passwordFieldOne.getPassword();
        convertedPassword = new String(password);
        
        if(this.textFieldUsername.getText().equals("") || convertedPassword.equals(""))
            JOptionPane.showMessageDialog(null, "It is not possible to leave any space in blanck");
        else{
            int counter = 0;
            for(int i = 0; i < MainFile.code; i++) {
                if(MainFile.users[i][1].equals(this.textFieldUsername.getText()) && MainFile.users[i][2].equals(convertedPassword)){
                    MainFile.userCode = i;
                    i = MainFile.code + 1;
                }
                else
                    counter++;
            }
            if(counter == MainFile.code){
                JOptionPane.showMessageDialog(null,"The username or the password are incorrect");
                this.textFieldUsername.setText("");
                this.passwordFieldOne.setText("");
            }
            else{
                JOptionPane.showMessageDialog(null,"Bienvenido al Juego " + MainFile.users[MainFile.userCode][1]+"!");
                
                UserLogedWindow userLoWindow = new UserLogedWindow();
                userLoWindow.setLocationRelativeTo(null);
                userLoWindow.setVisible(true);
                this.setVisible(false);
            }
        }
    }//GEN-LAST:event_btnLoginActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGoBack;
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel labelPassword;
    private javax.swing.JLabel labelUsername;
    private javax.swing.JPasswordField passwordFieldOne;
    private javax.swing.JTextField textFieldUsername;
    // End of variables declaration//GEN-END:variables
    CreateNewUser  newUser;
}
