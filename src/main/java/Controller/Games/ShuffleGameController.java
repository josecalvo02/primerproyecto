package Controller.Games;

import java.util.*;

public class ShuffleGameController {
    
    //This methos shuflles the ascending game
    public void shuffleBoard(int[][] board, int cols, int rows){
        Random rand = new Random();
        int[] array = new int[16];
        
        for(int i = 0; i < 16; i++){
            array[i] = i+1;
        }
        
        array[15] = -1;

        for(int i = 0; i < 15; i++) {
            int index =  rand.nextInt(15);
            int temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }

        int count = 0;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                board[i][j] = array[count];
                count = count + 1;
            }
        }
    }
    
    //This methos shuflles the descending game
    public void shuffleBoardTwo(int[][] board, int cols, int rows){
        Random rand = new Random();
        int[] array = new int[16];
        
        for(int i = 0; i < 16; i++){
            array[i] = i;
        }
        
        array[0] = -1;

        for(int i = 1; i < 16; i++) {
            int index =  rand.nextInt(15);
            int temp = array[i];
            array[i] = array[index];
            array[index] = temp;
        }

        int count = 0;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                board[i][j] = array[count];
                count = count + 1;
            }
        }
    }    
}