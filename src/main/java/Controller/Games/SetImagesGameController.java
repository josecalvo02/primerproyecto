package Controller.Games;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

public class SetImagesGameController {
    //Sets images for the game
    public void setImages(JFrame frame,JPanel mainPanel, JButton[][] button, JLabel[][] label, int[][] board, int cols, int rows){
        //Setting imagens to buttons
        for(int i = 0;i < rows; i++){
            for(int j = 0; j < cols; j++) {
                button[i][j] = new JButton();
                String text = i+","+j;
                button[i][j].setText(text);
                button[i][j].setFont(new Font("TimesRoman",Font.PLAIN,0));
                button[i][j].addActionListener((ActionListener) frame);

                int val = board[i][j];
                String fileName;
                
                if(val != -1){
                    fileName = "Pics/" + val + ".png";
                    label[i][j] = new JLabel(new ImageIcon(fileName), JLabel.CENTER);                
                }
                else{
                    label[i][j] = new JLabel("");                
                }
                button[i][j].add(label[i][j]);
                button[i][j].setBorder(BorderFactory.createLineBorder(Color.black,2));
                button[i][j].setBackground(Color.WHITE);
                mainPanel.add(button[i][j]);
            }
        }
    }
    
    public void setImagesSolvedOne(JFrame frame,JPanel mainPanel, JButton[][] button, JLabel[][] label, int[][] board, int cols, int rows){
        //Setting imagens to buttons
        for(int i = 0;i < rows; i++){
            for(int j = 0; j < cols; j++) {
                String text = i+","+j;
                button[i][j].setText(text);
                button[i][j].setFont(new Font("TimesRoman",Font.PLAIN,0));
                button[i][j].addActionListener((ActionListener) frame);

                int val = board[i][j];
                String fileName;
                
                if(val != -1){
                    fileName = "Pics/" + val + ".png";
                    label[i][j].setIcon(new ImageIcon(fileName));               
                }
                else
                    label[i][j].setIcon(null);         
                
                button[i][j].add(label[i][j]);
                button[i][j].setBorder(BorderFactory.createLineBorder(Color.black,2));
                button[i][j].setBackground(Color.WHITE);
            }
        }
    }
    
    public void setImagesSolvedTwo(JFrame frame,JPanel mainPanel, JButton[][] button, JLabel[][] label, int[][] board, int cols, int rows){
        //Setting imagens to buttons
        for(int i = 0;i < rows; i++){
            for(int j = 0; j < cols; j++) {
                String text = i+","+j;
                button[i][j].setText(text);
                button[i][j].setFont(new Font("TimesRoman",Font.PLAIN,0));
                button[i][j].addActionListener((ActionListener) frame);

                int val = board[i][j];
                String fileName;
                
                if(val != -1){
                    fileName = "Pics/" + val + ".png";
                    label[i][j].setIcon(new ImageIcon(fileName));               
                }
                else
                    label[i][j].setIcon(null);
                
                button[i][j].add(label[i][j]);
                button[i][j].setBorder(BorderFactory.createLineBorder(Color.black,2));
                button[i][j].setBackground(Color.WHITE);
            }
        }
    }
}
