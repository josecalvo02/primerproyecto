package Controller.Games;

public class GetRightPositions {
    public int getRightPos(int[][] board, int cols, int rows){
        int count = 0;
        int counter = 1;
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int  num = board[i][j];
                
                if(num != -1){
                    if(board[i][j] == counter){
                        counter++;
                        count++;
                    }
                    counter++;
                }
                else{
                    if(board[3][3] == -1){
                        count++;
                        counter++;
                    }
                    counter++;
                }
            }
        }
        return count;
    }
    
    public int getRightPosTwo(int[][] board, int cols, int rows){
        int count = 0;
        int counter = 16;
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int  num = board[i][j];
                
                if(num != -1){
                    if(board[i][j] == counter){
                        counter--;
                        count++;
                    }
                    counter--;
                }
                else{
                    if(board[0][0] == -1){
                        count++;
                    }
                    counter--;
                }
            }
        }
        return count;
    }
}
