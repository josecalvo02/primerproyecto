package Controller.Games;

public class CheckGameController {
    
    //Check if the ascending game is solved
    public Boolean isSolved(int[][] board, int cols, int rows){
        int count = 1;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(board[i][j] != count && board[i][j]!= -1){
                    return false;
                }
                count = count + 1;
            }
        }
        return true;
    }
    
    //Check if the descending game is solved
    public Boolean isSolvedTwo(int[][] board, int cols, int rows){
        int count = 16;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(board[i][j]!= count && board[i][j]!= -1){
                    return false;
                }
                count = count - 1;
            }
        }
        return true;
    }
}
