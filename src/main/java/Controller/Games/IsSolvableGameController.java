package Controller.Games;

public class IsSolvableGameController {
    
    //This method checks if the game is solvable
    public boolean isSolvable(int[][] board, int cols, int rows) {
        int countInversions = 0;
        int array[] = new int[board.length*board[0].length];
        
        for(int i = 0; i < rows; i++) {
            int[] row = board[i];
            for(int j = 0; j < cols; j++) {
                int number = board[i][j];
                array[i*row.length+j] = number;
            }
        }

        for (int i = 0; i < array.length-1; i++) {
            for (int j = 0; j < i; j++) {
              if (array[j] > array[i])
                countInversions++;
            }
        }
        return countInversions % 2 == 0;
    }
}
