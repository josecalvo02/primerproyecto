package Controller.Games;

public class SolveGameController {
    //Solve the ascending game
    public void solveGame(int[][] board, int cols, int rows){
        int[] array = new int[16];
        int count = 0;
        
        for(int i = 0; i < 16; i++)
            array[i] = i+1;
        
        array[15] = -1;
        
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < cols; j++)
                board[i][j] = array[count++]; 
    }
    
    //Solve the descending game
    public void solveGameTwo(int[][] board, int cols, int rows){
        int[] array = new int[16];
        int count = 0;
        
        for(int i = 0, j = 16; i < 16; i++)
            array[i] = j--;
        
        array[0] = -1;
        
        for(int i = 0; i < rows; i++)
            for(int j = 0; j < cols; j++){
                if(count == 0){
                    board[i][j] = array[0];
                    count++;
                }
                else
                    board[i][j] = array[count++];
            } 
        System.out.println(board[0][0]);
    }
}
