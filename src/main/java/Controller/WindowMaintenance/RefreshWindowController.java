package Controller.WindowMaintenance;

import javax.swing.*;

public class RefreshWindowController extends JFrame{
    public void refreshFrame(JFrame frame){
        SwingUtilities.updateComponentTreeUI(frame);
        synchronized(getTreeLock()) {
            validateTree();
        }
    }
}
