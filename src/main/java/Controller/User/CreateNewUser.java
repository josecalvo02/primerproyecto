package Controller.User;

import Views.MainFile;
import javax.swing.*;

public class CreateNewUser {
    
    public void createNewUserProfile(String name, String username, String password){
        try{
            MainFile.users[MainFile.code][0] = name;
            MainFile.users[MainFile.code][1] = username;
            MainFile.users[MainFile.code][2] = password;
            MainFile.code++;
            
            JOptionPane.showMessageDialog(null, "The user was created successfully");
        }
        catch(Exception error){
            JOptionPane.showMessageDialog(null, "There was an error while creating the user");
        }
    }
}