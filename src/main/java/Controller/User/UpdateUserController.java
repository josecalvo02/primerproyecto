package Controller.User;

import Views.MainFile;
import Controller.WindowMaintenance.*;
import javax.swing.*;

public class UpdateUserController {
    public void updateUser(JFrame frame,JLabel labelUsername, int opt){
        RefreshWindowController refresh = new RefreshWindowController();
        String tmp = "";
        if(opt == 0){
            tmp = JOptionPane.showInputDialog("Write your correct name");
            if(tmp.equals(""))
                JOptionPane.showMessageDialog(null, "It is not possible to leave the space in blanck");
            else
                MainFile.users[MainFile.userCode][0] = tmp;
        }
        else if(opt == 1){
            tmp = JOptionPane.showInputDialog("Write your new username");
            if(tmp.equals(""))
                JOptionPane.showMessageDialog(null, "It is not possible to leave the space in blanck");
            else{
                MainFile.users[MainFile.userCode][1] = tmp;
                labelUsername.setText(tmp);
                refresh.refreshFrame(frame);
            }
        }
        else if(opt == 2){
            tmp = JOptionPane.showInputDialog("Please write your current password to confirm");
            
            if(tmp.equals(MainFile.users[MainFile.userCode][2])){
                tmp = JOptionPane.showInputDialog("Confirm! Now write your new password");
                if(tmp.equals(""))
                    JOptionPane.showMessageDialog(null, "It is not possible to leave the space in blanck");
                else{
                    MainFile.users[MainFile.userCode][2] = tmp;
                    JOptionPane.showMessageDialog(null, "You have changed your password");
                }
            }
            else
                JOptionPane.showMessageDialog(null, "Your password is wrong");
        }  
    }
}