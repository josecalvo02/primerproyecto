package Controller.User;

import Views.MainFile;

public class DeleteUserController {

    public void deleteUser(){
        
        String[][] newArray = new String[10][3];
        
        for(int i = 0, j = 0; i < MainFile.users.length; i++){
            if(MainFile.userCode != i)
                newArray[j++] = MainFile.users[i];
        }
        MainFile.users = newArray;
        
        MainFile.code--;
    }
}