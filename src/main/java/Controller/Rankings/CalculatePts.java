package Controller.Rankings;

public class CalculatePts {
    public int calPts(int tmpTotalClicks, int tmpPts){
        if(tmpTotalClicks <= 50)
            tmpPts = 10;
        if(tmpTotalClicks <= 60)
            tmpPts = 9;
        if(tmpTotalClicks <= 70)
            tmpPts = 8;
        if(tmpTotalClicks <= 80)
            tmpPts = 7;
        if(tmpTotalClicks <= 90)
            tmpPts = 6;
        if(tmpTotalClicks <= 100)
            tmpPts = 5;
        if(tmpTotalClicks <= 110)
            tmpPts = 4;
        if(tmpTotalClicks <= 120)
            tmpPts = 3;
        if(tmpTotalClicks <= 130)
            tmpPts = 2;
        if(tmpTotalClicks > 130)
            tmpPts = 1;
        return tmpPts;
    }
}
