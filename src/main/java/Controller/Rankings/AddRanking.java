package Controller.Rankings;

import static Model.DBAmountOfClicks.*;
import static Model.DBRanking.*;
import Views.MainFile;
import java.util.Arrays;

public class AddRanking {
    public int tmpTotalClicks = 0;
    public int tmpPts = 0;
    public int tmpTimePts = 0;
    public int tmpTotalPts = 0;

    public void addNewRanking(int numLengthTwo, int tmpMinutes, int tmpSeconds, String giveUp, int counter){
        int[][] tmpArray = new int[1][6];
        String[][] tmpArrayUser = new String[1][2];
        
        for(int i = 0; i < topTenRankingUser.length; i++){
            if(topTenRankingUser[i][1] == null){
                topTenRankingUser[i][0] = "None";
                topTenRankingUser[i][1] = "";
            }
        }

        tmpArrayUser[0] = new String[]{MainFile.users[MainFile.userCode][1],
                                        giveUp};
        
        if(giveUp.equals("(Give Up)")!= true){
            calculateRanking(numLengthTwo, tmpMinutes);
            tmpArray[0] = new int[] {tmpMinutes,
                                    tmpSeconds,
                                    counterOfClicks[MainFile.userCode][numLengthTwo][numLengthTwo],
                                    counterOfWrongClicks[MainFile.userCode][numLengthTwo][numLengthTwo],
                                    tmpTotalClicks,
                                    tmpTotalPts};
        }
        else{
            tmpTotalPts = counter;
            tmpArray[0] = new int[] {tmpMinutes,
                                    tmpSeconds,
                                    counterOfClicks[MainFile.userCode][numLengthTwo][numLengthTwo],
                                    counterOfWrongClicks[MainFile.userCode][numLengthTwo][numLengthTwo],
                                    counterOfClicks[MainFile.userCode][numLengthTwo][numLengthTwo]+counterOfWrongClicks[MainFile.userCode][numLengthTwo][numLengthTwo],
                                    tmpTotalPts};
        }

        int[][] orderRankingArray = new int[10][6];
        String[][] orderRankingUsername = new String[10][2];
        
        if(giveUp.equals("(Give Up)") != true){
           for(int i = 0; i < topTenRanking.length; i++){
                //Is the position has more total points than the current game
                if(topTenRanking[i][5] > tmpArray[0][5]){
                    orderRankingArray[i] = topTenRanking[i];
                    orderRankingUsername[i] = topTenRankingUser[i];
                }

                //Is the position has same total points than the current game
                else if(topTenRanking[i][5] == tmpArray[0][5]){
                    //If the position has less min than the current game
                    if(topTenRanking[i][0] < tmpArray[0][0]){
                        orderRankingArray[i] = topTenRanking[i];
                        orderRankingUsername[i] = topTenRankingUser[i];
                    }

                    //If the position has same min than the current game
                    else if(topTenRanking[i][0] == tmpArray[0][0]){
                        //If the position has less sec than the current game
                        if(topTenRanking[i][1] < tmpArray[0][1]){
                            orderRankingArray[i] = topTenRanking[i];
                            orderRankingUsername[i] = topTenRankingUser[i];
                        }
                        
                        //If the position has same or more sec than the current game
                        else{
                            orderRankingArray[i] = tmpArray[0];
                            orderRankingUsername[i] = tmpArrayUser[0];
                            if(i != 9){
                                for(int j = i+1, k = i; j < topTenRanking.length; j++){
                                    orderRankingArray[j] = topTenRanking[k];
                                    orderRankingUsername[j] = topTenRankingUser[k++];
                                }
                            }
                            i = topTenRanking.length+1;
                        }
                    }

                    //If the position has less min than the current game
                    else{
                        orderRankingArray[i] = tmpArray[0];
                        orderRankingUsername[i] = tmpArrayUser[0];
                        if(i != 9){
                            for(int j = i+1, k = i; j < topTenRanking.length; j++){
                                orderRankingArray[j] = topTenRanking[k];
                                orderRankingUsername[j] = topTenRankingUser[k++];
                            }
                        }
                        i = topTenRanking.length+1;
                    }
                }

                //Is the position has less total points than the current game
                else{
                    orderRankingArray[i] = tmpArray[0];
                    orderRankingUsername[i] = tmpArrayUser[0];
                    if(i != 9){
                        for(int j = i+1, k = i; j < topTenRanking.length; j++){
                            orderRankingArray[j] = topTenRanking[k];
                            orderRankingUsername[j] = topTenRankingUser[k++];
                        }
                    }
                    i = topTenRanking.length+1;
                }
            } 
        }
        
        //If you give up
        else{
            for(int i = 0; i < topTenRanking.length; i++){
                if(topTenRankingUser[i][1].equals(giveUp) != false){
                    orderRankingArray[i] = topTenRanking[i];
                    orderRankingUsername[i] = topTenRankingUser[i];
                }
                else{
                    //Is the position has more total points than the current game
                    if(topTenRanking[i][5] > tmpArray[0][5]){
                        orderRankingArray[i] = topTenRanking[i];
                        orderRankingUsername[i] = topTenRankingUser[i];
                    }

                    //Is the position has same total points than the current game
                    else if(topTenRanking[i][5] == tmpArray[0][5]){
                        //If the position has less min than the current game
                        if(topTenRanking[i][0] < tmpArray[0][0]){
                            orderRankingArray[i] = topTenRanking[i];
                            orderRankingUsername[i] = topTenRankingUser[i];
                        }

                        //If the position has same min than the current game
                        else if(topTenRanking[i][0] == tmpArray[0][0]){
                            //If the position has less sec than the current game
                            if(topTenRanking[i][1] < tmpArray[0][1]){
                                orderRankingArray[i] = topTenRanking[i];
                                orderRankingUsername[i] = topTenRankingUser[i];
                            }
                            //If the position has same or more sec than the current game
                            else{
                                orderRankingArray[i] = tmpArray[0];
                                orderRankingUsername[i] = tmpArrayUser[0];
                                if(i != 9){
                                    for(int j = i+1, k = i; j < topTenRanking.length; j++){
                                        orderRankingArray[j] = topTenRanking[k];
                                        orderRankingUsername[j] = topTenRankingUser[k++];
                                    }
                                }
                                i = topTenRanking.length+1;
                            }
                        }

                        //If the position has less min than the current game
                        else{
                            orderRankingArray[i] = tmpArray[0];
                            orderRankingUsername[i] = tmpArrayUser[0];
                            if(i != 9){
                                for(int j = i+1, k = i; j < topTenRanking.length; j++){
                                    orderRankingArray[j] = topTenRanking[k];
                                    orderRankingUsername[j] = topTenRankingUser[k++];
                                }
                            }
                            i = topTenRanking.length+1;
                        }
                    }

                    //Is the position has less total points than the current game
                    else{
                        orderRankingArray[i] = tmpArray[0];
                        orderRankingUsername[i] = tmpArrayUser[0];
                        if(i != 9){
                            for(int j = i+1, k = i; j < topTenRanking.length; j++){
                                orderRankingArray[j] = topTenRanking[k];
                                orderRankingUsername[j] = topTenRankingUser[k++];
                            }
                        }
                        i = topTenRanking.length+1;
                    }  
                }
            }
        }
        
        topTenRanking = orderRankingArray;
        topTenRankingUser = orderRankingUsername;
    }
    
    public void calculateRanking(int numLengthTwo, int tmpMinutes){
        CalculatePts calculatePts = new CalculatePts();
        CalculateTime calculateTime = new CalculateTime();

        tmpTotalClicks = counterOfClicks[MainFile.userCode][numLengthTwo][numLengthTwo] + counterOfWrongClicks[MainFile.userCode][numLengthTwo][numLengthTwo];;
        
        tmpPts = calculatePts.calPts(tmpTotalClicks, tmpPts);
        tmpTimePts = calculateTime.calTime(tmpMinutes, tmpTimePts);
        tmpTotalPts = tmpPts + tmpTimePts;
    }
}
