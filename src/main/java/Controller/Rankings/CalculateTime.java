package Controller.Rankings;

public class CalculateTime {
    public int calTime(int tmpMinutes, int tmpTimePts){
        if(tmpMinutes <= 3)
            tmpTimePts = 10;
        if(tmpMinutes == 4)
            tmpTimePts = 9;
        if(tmpMinutes == 5)
            tmpTimePts = 8;
        if(tmpMinutes == 6)
            tmpTimePts = 7;
        if(tmpMinutes == 7)
            tmpTimePts = 6;
        if(tmpMinutes == 8)
            tmpTimePts = 5;
        if(tmpMinutes == 9)
            tmpTimePts = 4;
        if(tmpMinutes == 10)
            tmpTimePts = 3;
        if(tmpMinutes == 11)
            tmpTimePts = 2;
        if(tmpMinutes >= 12)
            tmpTimePts = 1;
        return tmpTimePts;
    }
}
