package Model;

public class DBRanking {
    public static int[][] topTenRanking = new int[10][6];
    public static String[][] topTenRankingUser = new String[10][2];
    
    public static void startRankingUser(){
        for(int i = 0; i < topTenRankingUser.length; i++){
            topTenRankingUser[i] = new String[]{"None","None"};
        }
    }
    public static void startRanking(){
        for(int i = 0; i < topTenRankingUser.length; i++){
            topTenRanking[i] = new int[]{0,0,0,0,0,0};
        }
    }
}