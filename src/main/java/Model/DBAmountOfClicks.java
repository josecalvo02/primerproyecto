package Model;

import Views.MainFile;

public class DBAmountOfClicks {
    public static int corClicks = 0;
    public static int incClicks = 0;
    
    public static int[][][] counterOfClicks = new int[MainFile.users.length][][];
    public static int[][][] counterOfWrongClicks = new int[MainFile.users.length][][];
    
    public static int[][][] counterOfMin = new int[MainFile.users.length][][];
    public static int[][][] counterOfSec = new int[MainFile.users.length][][];
    
    public static int[][] numOfGames = new int [MainFile.users.length][0];
    
}